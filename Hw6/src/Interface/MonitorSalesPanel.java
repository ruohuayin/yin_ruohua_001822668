/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.Calculation.MarketSalesmanRevenue;
import Business.Calculation.PersonTarget;
import Business.Calculation.ProductRevenue;
import Business.business;
import Business.market;
import Business.market_offer;
import Business.person;
import Business.product;
import Business.salesFunction.order;
import Business.salesFunction.order_item;
import Business.sales_person;
import Business.supplier;
import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author wendiyu
 */
public class MonitorSalesPanel extends javax.swing.JPanel {

    /**
     * Creates new form MonitorSalesPanel
     */
   
    business b;
    JPanel upc;
 
    ArrayList<MarketSalesmanRevenue> salesRank ;
    ArrayList<PersonTarget> personRevenueList;
    ArrayList<ProductRevenue> productRevenueList;

    MonitorSalesPanel(JPanel upc, business b) {
        initComponents();
        this.b = b;
        this.upc = upc;
        
        for(market m: b.getMl().getMa()){
            comboMarket.addItem(m.getName());
        }
        
        displayRevenueTotal();
        
        salesRank = new ArrayList();
        
        personRevenueList = new ArrayList();
        
        productRevenueList = new ArrayList();
        
        populateMarketRevenueTable();
        
        
        populateTop3Product();
        
    }
    
    public void populateTop3Product(){
    
        countProductRevenue();
        DefaultTableModel dtm = (DefaultTableModel) tblProduct.getModel();
            dtm.setRowCount(0);
             productRevenueList.sort((s1,s2) -> Integer.compare(s1.getProductGap(), s2.getProductGap()));
            Collections.reverse(productRevenueList);
        for (int i = 0;i<3;i++ ){
            
            Object[] row = new Object[4];
            row[0] = productRevenueList.get(i).getProduct().getID();
            row[1] = productRevenueList.get(i).getProduct().getDescription();
            row[2] = productRevenueList.get(i).getProductTotal();
            row[3] = productRevenueList.get(i).getProductGap();
            
           dtm.addRow(row);
            
        }
        
    }
    
    
    public void countProductRevenue(){
        for(supplier s: b.getSd().getSa()){
            for(product p: s.getPc().getAp()){
                ProductRevenue pr = new ProductRevenue(p.getID(),b);
                productRevenueList.add(pr);
            }
        }
        
        
        
    
    }
    
    
    public int countRevenueByMarket(String market){
        int totalRevenue = 0;
        
            for(person p: b.getPa()){
                if(p.getClass().getSimpleName().equals("sales_person")){
                    sales_person s = (sales_person) p;
                    for(order o: s.getOa()){
                        if(o.getMarket().equals(market)){
                            totalRevenue = totalRevenue + o.getTotal_price();
                        }
                }  
                }
        }
        System.out.println(totalRevenue);
        return totalRevenue;
        
    } 
    
    
    public int countRevenueGapByMarket(String market){
        int marketGapTotal = 0;
        
        for(person p: b.getPa()){
                if(p.getClass().getSimpleName().equals("sales_person")){
                    sales_person s = (sales_person) p;
                    for(order o: s.getOa()){
                        if(o.getMarket().equals(market)){
                            
                            for(order_item oi: o.getOia()){
                            market_offer mo = b.getMoc().findOffer(market,oi.getProduct());
                            marketGapTotal = marketGapTotal + (oi.getActualPrice()-mo.getTarget())*oi.getQuantity();
                                    
                                    }
                            
                        }
                    }
                }
        }
        
        return marketGapTotal;
    }
    
    
        public void populateMarketRevenueTable(){
                
            DefaultTableModel dtm = (DefaultTableModel) tblMarketRevenue.getModel();
            dtm.setRowCount(0);
        for (market m: b.getMl().getMa()){
            
            Object[] row = new Object[7];
            row[0] = m.getName();
            row[1] = countRevenueByMarket(m.getName());
            row[2] = countRevenueGapByMarket(m.getName());
            
           dtm.addRow(row);
            
        }
        
        }
    
    public void displayRevenueTotal(){
    
        int total = 0;  
        int gapTotal = 0;
        for(person p: b.getPa()){
            if(p.getClass().getSimpleName().equals("sales_person")){
                
                sales_person s = (sales_person) p;
                
                for(order o: s.getOa()){
                    total = total + o.getTotal_price();
                    for(order_item oi: o.getOia()){
                        market_offer mo = b.getMoc().findOffer(o.getMarket(), oi.getProduct());
                        gapTotal = gapTotal + (oi.getActualPrice()-mo.getTarget())*oi.getQuantity();
                    
                    }
                    
                 }
     
            }
        }
           txtTotal.setText(String.valueOf(total));
           txtGapTotal.setText(String.valueOf(gapTotal));
        
    }
    
    
    public void populatePersonTarget(){
        
            DefaultTableModel dtm = (DefaultTableModel) tblTarget.getModel();
            dtm.setRowCount(0);
            for (PersonTarget pt: personRevenueList){
            
            Object[] row = new Object[3];
            row[0] = pt.getPersonName();
            row[1] = pt.getTotalRevenue();
            row[2] = pt.getRevenueGap();
            
           dtm.addRow(row);
            
        }
    }
    
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSalesRank = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblProduct = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblMarketRevenue = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        comboMarket = new javax.swing.JComboBox<>();
        BtnMarketCheck = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblTarget = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        comboPerson = new javax.swing.JComboBox<>();
        BtnMarketCheck1 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtGapTotal = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();

        jLabel1.setText("Revenue Totals:");

        txtTotal.setEditable(false);

        jLabel5.setText("Market:");

        tblSalesRank.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Salesman", "Revenues", "Gap"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblSalesRank);

        tblProduct.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "productID", "ProductName", "Total Revenue", "Total Gap"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tblProduct);

        tblMarketRevenue.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Market", "Revenue Total", "Gap with target"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(tblMarketRevenue);

        jButton1.setText("< Back");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        BtnMarketCheck.setText("Check");
        BtnMarketCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnMarketCheckActionPerformed(evt);
            }
        });

        tblTarget.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Salesman", "Revenues", "Gap"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(tblTarget);

        jLabel2.setText("Sales person ");

        comboPerson.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Above", "Below" }));
        comboPerson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPersonActionPerformed(evt);
            }
        });

        BtnMarketCheck1.setText("Check");
        BtnMarketCheck1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnMarketCheck1ActionPerformed(evt);
            }
        });

        jLabel4.setText("Target");

        txtGapTotal.setEditable(false);

        jLabel8.setText("Top 3 product: ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(352, 352, 352)
                            .addComponent(jLabel3))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtGapTotal))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 331, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboPerson, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BtnMarketCheck1))
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 331, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(comboMarket, 0, 136, Short.MAX_VALUE)
                        .addGap(35, 35, 35)
                        .addComponent(BtnMarketCheck)
                        .addGap(101, 101, 101))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 331, Short.MAX_VALUE)
                            .addComponent(jLabel8)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(165, 165, 165)
                        .addComponent(jLabel3))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jButton1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel1)
                                            .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtGapTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(45, 45, 45)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5)
                                    .addComponent(comboMarket, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(BtnMarketCheck))))
                        .addGap(25, 25, 25)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(comboPerson, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)
                            .addComponent(BtnMarketCheck1)
                            .addComponent(jLabel8))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))))
                .addContainerGap(291, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void BtnMarketCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnMarketCheckActionPerformed
        // TODO add your handling code here:
        
        String market = comboMarket.getSelectedItem().toString();
        
        salesRank.clear();
        
        for(person p: b.getPa()){
            if(p.getClass().getSimpleName().equals("sales_person")){
                sales_person s = (sales_person) p;
                
                MarketSalesmanRevenue msr = new MarketSalesmanRevenue(market,s.getName(),b);
                salesRank.add(msr);

            }
        }
        
        populateSalesRank();
    }//GEN-LAST:event_BtnMarketCheckActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        upc.remove(this);
        CardLayout layout = (CardLayout) upc.getLayout();
        layout.previous(upc);
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void BtnMarketCheck1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnMarketCheck1ActionPerformed
        // TODO add your handling code here:
        String choose = comboPerson.getSelectedItem().toString();
        
        if(choose.equals("Above")){
            personRevenueList.clear();
            for(person p: b.getPa()){
                if(p.getClass().getSimpleName().equals("sales_person")){
                    sales_person s = (sales_person) p;
                    PersonTarget pt = new PersonTarget(s.getName(),b);
                    if(pt.getRevenueGap()>0)
                        personRevenueList.add(pt);
                }

            }
            
           
            populatePersonTarget();
            
        
        }else if(choose.equals("Below")){
            personRevenueList.clear();
            for(person p: b.getPa()){
                if(p.getClass().getSimpleName().equals("sales_person")){
                    sales_person s = (sales_person) p;
                    PersonTarget pt = new PersonTarget(s.getName(),b);
                    if(pt.getRevenueGap()<0)
                        personRevenueList.add(pt);
                }

            }
            
           
            populatePersonTarget();
            
            
            
            
        }
    }//GEN-LAST:event_BtnMarketCheck1ActionPerformed

    private void comboPersonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboPersonActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_comboPersonActionPerformed

    public void populateSalesRank(){
        DefaultTableModel dtm = (DefaultTableModel) tblSalesRank.getModel();
            dtm.setRowCount(0);
            salesRank.sort((s1,s2) -> Integer.compare(s1.getMarketRevenue(), s2.getMarketRevenue()));
            Collections.reverse(salesRank);
               
            
            for(int i=0;i<10;i++){
                Object[] row = new Object[3];
                row[0] = salesRank.get(i).getPersonName();
                row[1] = salesRank.get(i).getMarketRevenue();
                row[2] = salesRank.get(i).getRevenueGap();
            
                dtm.addRow(row);
            
            }
        
            
        
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnMarketCheck;
    private javax.swing.JButton BtnMarketCheck1;
    private javax.swing.JComboBox<String> comboMarket;
    private javax.swing.JComboBox<String> comboPerson;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable tblMarketRevenue;
    private javax.swing.JTable tblProduct;
    private javax.swing.JTable tblSalesRank;
    private javax.swing.JTable tblTarget;
    private javax.swing.JTextField txtGapTotal;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables
}
