/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Interface.Admin;

import Business.business;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author qwe09
 */
public class mmoJPanel extends javax.swing.JPanel {

    /** Creates new form mmoJPanel */
    business b;
    private JPanel userProcessContainer;
    
    mmoJPanel(business b, JPanel userProcessContainer) {
        initComponents();
        this.b=b;
        this.userProcessContainer=userProcessContainer;
        pop();
    }

     public void pop() {
        DefaultTableModel dtm=(DefaultTableModel) MOT.getModel();
        dtm.setRowCount(0);
        for(int i=0;i<b.getMoc().getMarketOfferList().size();i++)
        {
            dtm.addRow(new Object[] {});
            MOT.setValueAt(b.getMoc().getMarketOfferList().get(i).getMarket(), i, 0);
            MOT.setValueAt(b.getMoc().getMarketOfferList().get(i).getP().getName(), i, 1);
            MOT.setValueAt(b.getMoc().getMarketOfferList().get(i).getP().getID(), i, 2);
            MOT.setValueAt(b.getMoc().getMarketOfferList().get(i).getFloor(), i, 3);
            MOT.setValueAt(b.getMoc().getMarketOfferList().get(i).getCeiling(), i, 4);
            MOT.setValueAt(b.getMoc().getMarketOfferList().get(i).getTarget(), i, 5);
        }
    }
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        MOT = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();

        MOT.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Market", "Product Name", "Product ID", "Floor", "Ceiling", "Target"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, false, true, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(MOT);
        if (MOT.getColumnModel().getColumnCount() > 0) {
            MOT.getColumnModel().getColumn(0).setResizable(false);
            MOT.getColumnModel().getColumn(1).setResizable(false);
            MOT.getColumnModel().getColumn(2).setResizable(false);
            MOT.getColumnModel().getColumn(3).setResizable(false);
            MOT.getColumnModel().getColumn(4).setResizable(false);
            MOT.getColumnModel().getColumn(5).setResizable(false);
        }

        jButton1.setText("Add");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Delete");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Update");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton5.setText("<Back");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 734, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton3)
                    .addComponent(jButton5))
                .addContainerGap(102, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addGap(18, 18, 18)
                .addComponent(jButton3)
                .addGap(18, 18, 18)
                .addComponent(jButton5)
                .addContainerGap(182, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        int select=MOT.getSelectedRow();
        if(select<0)
        {
            JOptionPane.showMessageDialog(null, "Please select a row first", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        else
        {
            for(int i=0;i<b.getMoc().getMarketOfferList().size();i++)
            {
                if(MOT.getValueAt(select, 0).equals(b.getMoc().getMarketOfferList().get(i).getMarket()) && MOT.getValueAt(select, 2).equals(b.getMoc().getMarketOfferList().get(i).getP().getID()))
                {
                    b.getMoc().getMarketOfferList().remove(i);
                }
            }
            pop();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        CardLayout layout=(CardLayout) userProcessContainer.getLayout();
        amoJPanel panel=new amoJPanel(b, userProcessContainer);
        userProcessContainer.add("amoJPanel", panel);
        layout.next(userProcessContainer);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        int select=MOT.getSelectedRow();
        if(select<0)
        {
            JOptionPane.showMessageDialog(null, "Please select a row first", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        else
        {
        CardLayout layout=(CardLayout) userProcessContainer.getLayout();
        umoJPanel panel=new umoJPanel(b, userProcessContainer, MOT.getValueAt(select, 0), MOT.getValueAt(select, 2));
        userProcessContainer.add("umoJPanel", panel);
        layout.next(userProcessContainer);
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component = componentArray[componentArray.length - 1];
        adminJPanel manageAccountJPanel = (adminJPanel) component;
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_jButton5ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable MOT;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton5;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
