/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Calculation;

import Business.business;
import Business.market_offer;
import Business.person;
import Business.salesFunction.order;
import Business.salesFunction.order_item;
import Business.sales_person;

/**
 *
 * @author Yin
 */
public class MarketSalesmanRevenue {
    
    String personName;
    int MarketRevenue = 0;
    int RevenueGap = 0;

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public int getMarketRevenue() {
        return MarketRevenue;
    }

    public void setMarketRevenue(int MarketRevenue) {
        this.MarketRevenue = MarketRevenue;
    }

    public int getRevenueGap() {
        return RevenueGap;
    }

    public void setRevenueGap(int RevenueGap) {
        this.RevenueGap = RevenueGap;
    }

    
    
    
    public MarketSalesmanRevenue(String market,String personName,business b) {
        this.personName = personName;
        
        for(person p: b.getPa()){
            if(p.getName().equals(personName)){
                sales_person s = (sales_person) p;
                    for(order o: s.getOa()){
                        if(o.getMarket().equals(market)){
                            
                            MarketRevenue = MarketRevenue + o.getTotal_price();
                            
                            for(order_item oi: o.getOia()){
                                
                                market_offer mo = b.getMoc().findOffer(market, oi.getProduct());
                                
                                RevenueGap = RevenueGap + (oi.getActualPrice()-mo.getTarget()) * oi.getQuantity();
                                
                            }
                        }
                    
                    }
            }
        
        }
        
        System.out.println(MarketRevenue);
        
    }
       
}
