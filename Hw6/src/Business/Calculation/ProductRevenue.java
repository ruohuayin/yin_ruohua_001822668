/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Calculation;

import Business.business;
import Business.market_offer;
import Business.person;
import Business.product;
import Business.product_catalog;
import Business.salesFunction.order;
import Business.salesFunction.order_item;
import Business.sales_person;
import Business.supplier;

/**
 *
 * @author Yin
 */
public class ProductRevenue {
    product product;
    int productTotal=0;
    int productGap=0;
    
    public ProductRevenue(String productID, business b){
    
       for(supplier s: b.getSd().getSa()){
           for(product p: s.getPc().getAp()){
               if(p.getID().equals(productID)){
                   product = p;
               }
           }
       }
       
       for(person person: b.getPa()){
           if(person.getClass().getSimpleName().equals("sales_person")){
               sales_person s  =  (sales_person) person;
               for(order o: s.getOa()){
                   for(order_item oi: o.getOia()){
                       if(oi.getProduct().getID().equals(productID)){
                           market_offer mo = b.getMoc().findOffer(o.getMarket(), product);
                           productTotal = productTotal + oi.getActualPrice() * oi.getQuantity();
                           productGap = productGap + (oi.getActualPrice()-mo.getTarget())*oi.getQuantity();
                       
                       }
                   }
               }
           
           }
       }
        
    
    }

    public product getProduct() {
        return product;
    }

    public void setProduct(product product) {
        this.product = product;
    }

    public int getProductTotal() {
        return productTotal;
    }

    public void setProductTotal(int productTotal) {
        this.productTotal = productTotal;
    }

    public int getProductGap() {
        return productGap;
    }

    public void setProductGap(int productGap) {
        this.productGap = productGap;
    }
    
    
    
}
