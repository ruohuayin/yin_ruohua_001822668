/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author Yin
 */
public class AirplaneFleet {
    
    ArrayList<Airplane> airplaneList;
    
    public AirplaneFleet(){
        airplaneList = new ArrayList();
    }

    public ArrayList<Airplane> getAirplaneList() {
        return airplaneList;
    }

    public void setAirplaneList(ArrayList<Airplane> airplaneList) {
        this.airplaneList = airplaneList;
    }
    
    public Airplane addAirplane(){
        Airplane a = new Airplane();
        airplaneList.add(a);
        return a;
    }
    
    public void deleteAirplane(Airplane a){
        airplaneList.remove(a);
    }
    
    
    
}
