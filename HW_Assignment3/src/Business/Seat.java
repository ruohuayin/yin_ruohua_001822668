/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Yin
 */
public class Seat {
    
    String name;    // 1A  1B 1C
    int row; //1-25
    int column; // ABCEDF
    double price;
    String desc; // window, middle, aisle
    Boolean occupation = false;
    Customer inseatCustomer = null;
    
    public void registerSeat(Customer c){
        this.inseatCustomer = c;
        occupation = true;
    }
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Boolean getOccupation() {
        return occupation;
    }

    public void setOccupation(Boolean occupation) {
        this.occupation = occupation;
    }
    
       
}

    
    
    
    

