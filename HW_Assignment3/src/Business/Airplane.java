/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Yin
 */
public class Airplane {
    
    String airplaneSerial;
    String Manufacturer;
    String model;
    int seatNum;
    String abbreName;
    
    
    public String getAirplaneSerial() {
        return airplaneSerial;
    }

    public void setAirplaneSerial(String airplaneSerial) {
        this.airplaneSerial = airplaneSerial;
    }

    public String getManufacturer() {
        return Manufacturer;
    }

    public void setManufacturer(String Manufacturer) {
        this.Manufacturer = Manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getSeatNum() {
        return seatNum;
    }

    public void setSeatNum(int seatNum) {
        this.seatNum = seatNum;
    }

    public String getAbbreName() {
        return abbreName;
    }

    public void setAbbreName(String abbreName) {
        this.abbreName = abbreName;
    }
    

}
