/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author Yin
 */
public class AirlinerDirectory {
    
    ArrayList<Airliner> airlinerDirectory;
    
    public AirlinerDirectory(){
        airlinerDirectory = new ArrayList();
    }

    public Airliner addAirliner(){
        Airliner al = new Airliner();
        airlinerDirectory.add(al);
        return al;
    }
    
    
    public ArrayList<Airliner> getAirlinerDirectory() {
        return airlinerDirectory;
    }

    public void setAirlinerDirectory(ArrayList<Airliner> airlinerDirectory) {
        this.airlinerDirectory = airlinerDirectory;
    }
    
    
    
    public ArrayList load_AirlinerDirectory(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader("Data_csv/Airliner.csv"));//换成你的文件名   
            String line = null; 
            while((line=reader.readLine())!=null){
                // AirlinerDirectory AD = new AirlinerDirectory();
               String item[] = line.split(",");
               Airliner al = new Airliner();
               al.setAirlinerName(item[0]);
               al.setAbbreName(item[1]);
               airlinerDirectory.add(al);
            }    
        } catch (Exception e) {    
            e.printStackTrace();    
        }
        return airlinerDirectory;
    }
    
     
        
    
    
    
    
    
    
    
}
