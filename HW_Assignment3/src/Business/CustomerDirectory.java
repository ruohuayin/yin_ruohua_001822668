/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author Yin
 */
public class CustomerDirectory {
    
    ArrayList<Customer> customerDirectory;
    
    public CustomerDirectory(){
        customerDirectory = new ArrayList();
    }

    public ArrayList<Customer> getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(ArrayList<Customer> customerDirectory) {
        this.customerDirectory = customerDirectory;
    }
    
    public Customer addCustomer(){
        Customer c = new Customer();
        customerDirectory.add(c);
        return c;
    }
    public void deleteCustomer(Customer c){
        customerDirectory.remove(c);
    }
    
    
    public ArrayList load_CustomerDirectory(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader("Data_csv/CustomerDirectory.csv"));//换成你的文件名   
            String line = null;
            while((line=reader.readLine())!=null){
                // AirlinerDirectory AD = new AirlinerDirectory();
               String item[] = line.split(",");
               Customer c = new Customer();
               c.setName(item[0]);
               c.setGender(item[1]);
               c.setCustomerID(item[2]);
               customerDirectory.add(c);
            }    
        } catch (Exception e) {    
            e.printStackTrace();    
        }
        return customerDirectory;
    
    }
}
