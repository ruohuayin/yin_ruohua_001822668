/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Yin
 */
public class TravelAgency {
    
    AirlinerDirectory AD;
    CustomerDirectory CD;
    FlightMaster FM;
    
    public TravelAgency(){
        AD = new AirlinerDirectory();
        CD = new CustomerDirectory();
        FM = new FlightMaster();
    }

    public AirlinerDirectory getAD() {
        return AD;
    }

    public void setAD(AirlinerDirectory AD) {
        this.AD = AD;
    }

    public CustomerDirectory getCD() {
        return CD;
    }

    public void setCD(CustomerDirectory CD) {
        this.CD = CD;
    }

    public FlightMaster getFM() {
        return FM;
    }

    public void setFM(FlightMaster FM) {
        this.FM = FM;
    }

    
    
  
    
    
    
    
    
    
    

    
    
    
    
    
    
}
