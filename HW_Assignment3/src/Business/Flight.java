/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author Yin
 */
public class Flight {
    
    
    String Serial;  //CA
    String departuretime;   // 10.01.1993.1535
    String landingTime;     // 10.02.1994.1535
    String departureLoc;    // BOS
    String landingLoc;      // LAX
    
    SeatsAssignment sa;

    public Flight(){
        sa = new SeatsAssignment();
    }
    
    public String getSerial() {
        return Serial;
    }

    public void setSerial(String Serial) {
        this.Serial = Serial;
    }

    public String getDeparturetime() {
        return departuretime;
    }

    public void setDeparturetime(String departuretime) {
        this.departuretime = departuretime;
    }

    public String getLandingTime() {
        return landingTime;
    }

    public void setLandingTime(String landingTime) {
        this.landingTime = landingTime;
    }

    public String getDepartureLoc() {
        return departureLoc;
    }

    public void setDepartureLoc(String departureLoc) {
        this.departureLoc = departureLoc;
    }

    public String getLandingLoc() {
        return landingLoc;
    }

    public void setLandingLoc(String landingLoc) {
        this.landingLoc = landingLoc;
    }

    public SeatsAssignment getSa() {
        return sa;
    }

    public void setSa(SeatsAssignment sa) {
        this.sa = sa;
    }
    
    
    public ArrayList load_seatAssignment(){
        ArrayList<Seat> seat_List = new ArrayList();
        int i;
        int j;
        try {
            BufferedReader reader = new BufferedReader(new FileReader("Data_csv/SeatAssignment.csv"));//换成你的文件名   
            String line = null; 
            while((line=reader.readLine())!=null){
               String item[] = line.split(",");
               if(item[0].equals(Serial)){
                   int price = Integer.parseInt(item[1]);
                   double windowRate = Double.parseDouble(item[2]);
                   double middleRate = Double.parseDouble(item[3]);
                   double aisleRate = Double.parseDouble(item[4]);
               
                   for(i=0;i<25;i++){
                           for(j=0;j<6;j++){
                                Seat s = new Seat();    
                                s.setRow(i+1);
                                s.setColumn(j+1);
                                s.setName(s.getRow()+"."+s.getColumn());
                                s.occupation = false;
                                if(j==1 || j==6 ){
                                    s.setPrice(price*windowRate);
                                    s.setDesc("window");
                                }else if(j==2 || j==5){
                                    s.setPrice(price*middleRate);
                                    s.setDesc("middle");
                                }else {
                                    s.setPrice(price*aisleRate);
                                    s.setDesc("aisle");
                                }
                                seat_List.add(s);
                            }
                   }
               }
               
               }
     
              
        } catch (Exception e) {    
            e.printStackTrace();    
        }
        return seat_List;
    }
    
    
    
    
    
    
}
