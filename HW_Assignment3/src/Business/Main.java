/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author Yin
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    
    
    
    public static void main(String[] args) {
        
        
        TravelAgency TA = new TravelAgency();       //new TA
//设定 Airliner Directory        
        AirlinerDirectory AD  = TA.getAD();
        CustomerDirectory CD = TA.getCD();
        FlightMaster FM = TA.getFM();
       
//获取数据的同时，给 AirlinerDirectory的 Arraylist赋值
        ArrayList<Airliner> airliner_List = AD.load_AirlinerDirectory();
        
        for(Airliner al: AD.getAirlinerDirectory()){
            System.out.println("AirlinerName : " + al.getAirlinerName() + "  ||||| Abbreviation Name : " + al.getAbbreName());
        }
        
//设定两个 Airliner       
        Airliner AA = AD.getAirlinerDirectory().get(0);
    //    AA.setAbbreName("AA");
        Airliner CA = AD.getAirlinerDirectory().get(1);
      //  CA.setAbbreName("CA");
        
//load两个 planeList       
        ArrayList<Airplane> AAplane_List = AA.load_AirplaneFleet();
        ArrayList<Airplane> CAplane_List = CA.load_AirplaneFleet();

//把以上两个ArrayList分别赋值给对应的AF
        AA.getAF().setAirplaneList(AAplane_List);
        CA.getAF().setAirplaneList(CAplane_List);
        
//打印两个航空公司分别拥有的飞机信息       
      System.out.println("American Airline has: ");
        for(Airplane ap: AA.getAF().getAirplaneList()){
            System.out.println("Plane Serial: " + ap.airplaneSerial +"  Plane type: " + ap.Manufacturer+ap.model + "   with " + ap.seatNum +" " +" seats  " );
        }
      System.out.println("Air China has: ");
        for(Airplane ap: CA.getAF().getAirplaneList()){
            System.out.println("Plane Serial: " + ap.airplaneSerial +"  Plane type: " + ap.Manufacturer+ap.model + "   with " + ap.seatNum +" " +" seats  " );
        }
        
   //     System.out.println(TA.getAD().getAirlinerDirectory().get(1).getAF().getAirplaneList().get(1).getAirplaneSerial());
        
  // 
       ArrayList<Flight> AAflight_List= TA.getAD().airlinerDirectory.get(0).getFS().load_FlightSchedule("AA");
       ArrayList<Flight> CAflight_List= TA.getAD().airlinerDirectory.get(1).getFS().load_FlightSchedule("CA");
       
       for(Flight f: AA.getFS().getFlightSchedule()){
           System.out.println("Flight Serial: " + f.Serial+" departure time: " +f.departuretime + " - " + "landing time: " + f.landingTime +" from " + f.departureLoc+" to " + f.landingLoc);
       }
       
       for(Flight f: CA.getFS().getFlightSchedule()){
           System.out.println("Flight Serial: " + f.Serial+" departure time: " +f.departuretime + " - " + "landing time: " + f.landingTime +" from " + f.departureLoc+" to " + f.landingLoc);
       }
       
       System.out.println(AAflight_List);
       System.out.println(CAflight_List);
      
       FlightSchedule AAflightSchedule = AA.getFS();
       FlightSchedule CAflightSchedule = CA.getFS();     
                                           //CA 的第一个flight             CA第一个seatAssignment
       ArrayList<Seat> seatA_CA1234 = CAflightSchedule.getFlightSchedule().get(0).load_seatAssignment();
       ArrayList<Seat> seatA_CA2013 = CAflightSchedule.getFlightSchedule().get(0).load_seatAssignment();
       ArrayList<Seat> seatA_AA2626 = CAflightSchedule.getFlightSchedule().get(1).load_seatAssignment();
       ArrayList<Seat> seatA_AA1315 = CAflightSchedule.getFlightSchedule().get(1).load_seatAssignment();
        
       FM.getFlightMaster().add(AAflightSchedule);
       FM.getFlightMaster().add(CAflightSchedule);
       
      
//加载CustomerDirectory     
        CD.load_CustomerDirectory();
        System.out.println(CD.getCustomerDirectory().get(15).getName());
        ArrayList<Customer> cust_List = CD.getCustomerDirectory();
        
 //给CA_1234随机分配座位      
       for(Customer c: cust_List){
           int row = (int)(Math.random()*25) + 1;
           int column = (int)(Math.random()*6) +1;
           String seatName_ran = row+"."+column;
               for(Seat s: seatA_CA1234){
                   if(s.getName().equals(seatName_ran) && s.occupation==false){
                       s.registerSeat(c);
                       System.out.println(s.name+" is occupied:"+s.occupation+" by " + c.Name);
                       
                   }      
              }        
 
           }
       
  Double Revenue_CA1234 =0.00;
  Double Revenue_CA2013 =0.00;
  Double Revenue_AA2626 =0.00;
  Double Revenue_AA1315 =0.00;
 //给CA_2013随机分配座位      
       for(Customer c: cust_List){
           int row = (int)(Math.random()*25) + 1;
           int column = (int)(Math.random()*6) +1;
           String seatName_ran = row+"."+column;
               for(Seat s: seatA_CA2013){
                   if(s.getName().equals(seatName_ran) && s.occupation==false){
                       s.registerSeat(c);
                       Revenue_CA2013 = Revenue_CA2013+ s.getPrice();
                       System.out.println(s.name+" is occupied:"+s.occupation+" by " + c.Name);
                       
                   }      
              }        
 
           }
       //给AA_2626随机分配座位      
       for(Customer c: cust_List){
           int row = (int)(Math.random()*25) + 1;
           int column = (int)(Math.random()*6) +1;
           String seatName_ran = row+"."+column;
               for(Seat s: seatA_AA2626){
                   if(s.getName().equals(seatName_ran) && s.occupation==false){
                       s.registerSeat(c);
                       Revenue_AA2626 = Revenue_AA2626+ s.getPrice();
                       System.out.println(s.name+" is occupied:"+s.occupation+" by " + c.Name);
                       
                   }      
              }        
 
           }
       //给AA_1315随机分配座位      
       for(Customer c: cust_List){
           int row = (int)(Math.random()*25) + 1;
           int column = (int)(Math.random()*6) +1;
           String seatName_ran = row+"."+column;
               for(Seat s: seatA_AA1315){
                   if(s.getName().equals(seatName_ran) && s.occupation==false){
                       s.registerSeat(c);
                       Revenue_AA1315 = Revenue_AA1315+ s.getPrice();
                       System.out.println(s.name+" is occupied:"+s.occupation+" by " + c.Name);
                       
                   }      
              }        
 
           }
      
  
       System.out.println("                    //s\\                    ");
       System.out.println("                  //     \\                    ");
       System.out.println("                  // 747 \\                  ");
       System.out.println("                 //       \\                  ");
       System.out.println("                // Boeing  \\                  ");
       System.out.println("              //             \\                  ");
       System.out.println("            // SeatNum: 150    \\                  ");
       System.out.println("          //    CA1234           \\                  ");
       System.out.println("        // *A*|*B*|*C*|*D*|*E*|*F  \\                  ");
         System.out.println("");
         
         
//Draw the seat allocation diagram for CA_1234
       int m = 0;
       int n = 0;
       
       for(Seat s: seatA_CA1234){
            for(m=0;m<25;m++){
                for(n=0;n<6;n++){
                    if(s.getRow()==m+1 && s.getColumn() == n+1){
                        while(n%6==0){
         System.out.print("       ["+(m+1)+"] ");
                        break;
                        }
                        if(s.occupation ==false){
                            System.out.print(" □  ");
                    }else{
                        System.out.print(" ☆  ");
                        Revenue_CA1234 = Revenue_CA1234+s.getPrice();
                        }
                        while(n%6 == 5){
                            System.out.println(" ");
                            break;}
                  }  
                   
                }//end for n
                
            }//end for m
                
       }//end for seatAssignment_CA1234
       double Revenue_AA = Revenue_CA1234+Revenue_CA2013;
       double Revenue_CA = Revenue_AA2626+Revenue_AA1315;
       
       double Revenue_all = Revenue_AA+Revenue_CA;

       
       Flight a = AAflightSchedule.getFlightSchedule().get(0);
       Flight b = AAflightSchedule.getFlightSchedule().get(1);
       Flight c = CAflightSchedule.getFlightSchedule().get(0);
       Flight d = AAflightSchedule.getFlightSchedule().get(1);

       System.out.println("        ******************************☆☆☆☆☆☆☆☆☆☆☆☆☆               TOTAL REVENUES                     ☆☆☆☆☆☆☆☆☆☆☆☆☆***********************************");
       System.out.println("        **********                                      ------  Per Flight ------                                                     ***************");
       System.out.println("        **********     Flight: "+a.getSerial() +" :  $"+ Revenue_CA1234 +" From["+a.getDepartureLoc()+"] to ["+a.getLandingLoc()+"] Departure At "+a.getDeparturetime()+" - "+a.getLandingTime()+"          ***************");
       System.out.println("        **********     Flight: "+b.getSerial() +" :  $"+ Revenue_CA2013 +" From["+b.getDepartureLoc()+"] to ["+b.getLandingLoc()+"] Departure At "+b.getDeparturetime()+" - "+b.getLandingTime()+"              ***************");
       System.out.println("        **********     Flight: "+c.getSerial() +" :  $"+ Revenue_AA2626 +" From["+c.getDepartureLoc()+"] to ["+c.getLandingLoc()+"] Departure At "+c.getDeparturetime()+" - "+c.getLandingTime()+"              ***************");
       System.out.println("        **********     Flight: "+d.getSerial() +" :  $"+ Revenue_AA1315 +" From["+d.getDepartureLoc()+"] to ["+d.getLandingLoc()+"] Departure At "+d.getDeparturetime()+" - "+d.getLandingTime()+"              ***************");
       System.out.println("        ☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆");
       System.out.println("        **********                                      ------  Total for all Flight ------                                           ***************");
       System.out.println("        **********      " + "There are " + TA.getAD().getAirlinerDirectory().size() + " Airliners in the Directory, with a total revenue: $" + Revenue_all +  
                                                                                                                        " for  " + (AAflightSchedule.getFlightSchedule().size()+AAflightSchedule.getFlightSchedule().size() )+" Flights"     +"                            ************")  ;   
       System.out.println("        ☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆");
       System.out.println("        **********                                      ------  Total for Each Airliner ------                                        ***************");   
       System.out.println("        **********                                                "+ TA.getAD().getAirlinerDirectory().size() + " Airliners included in the Directory," + "                              ***************");                                                                                                                                                                                                           
       System.out.println("        **********                              ☆☆☆☆☆☆☆"+ TA.getAD().getAirlinerDirectory().get(0).getAirlinerName()+ "☆☆☆☆☆☆☆  total Avenue: $"+ Revenue_AA +"                               ***************"); 
       System.out.println("        **********                              ☆☆☆☆☆☆☆   "+ TA.getAD().getAirlinerDirectory().get(1).getAirlinerName()+ "   ☆☆☆☆☆☆☆    total Avenue: $"+ Revenue_CA +"                               ***************"); 
       System.out.println("        ☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆");
    }
    
   // TA.getAD().getAirlinerDirectory().size() + " Airliners in the Directory, with a total revenue: $"
   
    
    
    
}
