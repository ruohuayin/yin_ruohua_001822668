/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author Yin
 */
public class Airliner {
    
    
    String airlinerName;
    String abbreName;
    AirplaneFleet AF;
    FlightSchedule FS;
    
    public Airliner(){
        
        AF = new AirplaneFleet();
        FS = new FlightSchedule();
        
      //  this.airlinerName = airlinerName;
      //  this.abbreName = abbreName;
        
    }

    public String getAirlinerName() {
        return airlinerName;
    }

    public void setAirlinerName(String airlinerName) {
        this.airlinerName = airlinerName;
    }

    public String getAbbreName() {
        return abbreName;
    }

    public void setAbbreName(String abbreName) {
        this.abbreName = abbreName;
    }

    public AirplaneFleet getAF() {
        return AF;
    }

    public void setAF(AirplaneFleet AF) {
        this.AF = AF;
    }

    public FlightSchedule getFS() {
        return FS;
    }

    public void setFS(FlightSchedule FS) {
        this.FS = FS;
    }
    
    
    public ArrayList load_AirplaneFleet(){
         ArrayList<Airplane> apf = new ArrayList();
         String a = this.abbreName;
        try {
            BufferedReader reader = new BufferedReader(new FileReader("Data_csv/"+a+"planeFleet.csv"));//换成你的文件名   
            String line = null;
            while((line=reader.readLine())!=null){
                // AirlinerDirectory AD = new AirlinerDirectory();
               String item[] = line.split(",");
               Airplane ap = new Airplane();
               ap.setManufacturer(item[0]);
               ap.setModel(item[1]);
               ap.setAirplaneSerial(item[2]);
               ap.setSeatNum(Integer.parseInt(item[3]));
               ap.setAbbreName(item[4]);
               apf.add(ap); 
            }    
        } catch (Exception e) {    
            e.printStackTrace();
        }
            return apf;
    }

    
    
    
    
    
    
}
