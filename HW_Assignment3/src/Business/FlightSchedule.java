/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author Yin
 */
public class FlightSchedule {
    
    
    ArrayList<Flight> flightSchedule;
    
    public FlightSchedule(){
        flightSchedule = new ArrayList();
    }

    public ArrayList<Flight> getFlightSchedule() {
        return flightSchedule;
    }

    public void setFlightSchedule(ArrayList<Flight> flightSchedule) {
        this.flightSchedule = flightSchedule;
    }

   public ArrayList load_FlightSchedule(String abbreName){
       
       try {
            BufferedReader reader = new BufferedReader(new FileReader("Data_csv/"+abbreName+"flight.csv"));//换成你的文件名   
            String line = null; 
            while((line=reader.readLine())!=null){
               String item[] = line.split(",");
               Flight f = new Flight(); 
               f.setSerial(item[0]);
               f.setDeparturetime(item[1]);
               f.setLandingTime(item[2]);
               f.setDepartureLoc(item[3]);
               f.setLandingLoc(item[4]);
               flightSchedule.add(f);
            }
        } catch (Exception e) {    
            e.printStackTrace();    
        }
        return flightSchedule;
    }
    
}
