/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Yin
 */
public class timeCount {
    
    String timeF;

    public String getTimeSt() {
        return timeF;
    }

    public void setTimeSt(String timeF) {
        this.timeF = timeF;
    }
    
    public String refresh(){
        
        String timeSt = String.valueOf(System.currentTimeMillis());   
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = new Long(timeSt);
        Date date = new Date(lt);
        timeF = simpleDateFormat.format(date);
        return timeF;
        
    }
}
