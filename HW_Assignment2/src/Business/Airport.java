/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Yin
 */
public class Airport {
    
    String AirportName;
    String abbreName;
    

    public String getAirportName() {
        return AirportName;
    }

    public void setAirportName(String AirportName) {
        this.AirportName = AirportName;
    }

    public String getAbbreName() {
        return abbreName;
    }

    public void setAbbreName(String abbreName) {
        this.abbreName = abbreName;
    }
    
    
}
