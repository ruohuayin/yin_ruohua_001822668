/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Yin
 */
public class Airplane {
    
    String serialNum; //key
    
    Boolean available;
    String availDate;
    int seatNum;
    
    String manufacturer;
    String manuDate;
    String Model;
        
    String airliner;
    
    String expiredWarrantyDate;
    
    Airport airport;
    
    public Airplane(){
        airport = new Airport();
    }

    public Airport getAirport() {
        return airport;
    }

    public void setAirport(Airport airport) {
        this.airport = airport;
    }

    

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public String getAvailDate() {
        return availDate;
    }

    public void setAvailDate(String availDate) {
        this.availDate = availDate;
    }

    public int getSeatNum() {
        return seatNum;
    }

    public void setSeatNum(int seatNum) {
        this.seatNum = seatNum;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getManuDate() {
        return manuDate;
    }

    public void setManuDate(String manuDate) {
        this.manuDate = manuDate;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String Model) {
        this.Model = Model;
    }

    public String getAirliner() {
        return airliner;
    }

    public void setAirliner(String airliner) {
        this.airliner = airliner;
    }

    public String getExpiredWarrantyDate() {
        return expiredWarrantyDate;
    }

    public void setExpiredWarrantyDate(String expiredWarrantyDate) {
        this.expiredWarrantyDate = expiredWarrantyDate;
    }
    
    public String toString(){
       return this.manufacturer;
    
    }
    

           
    
    
    
}
