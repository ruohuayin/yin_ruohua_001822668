/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Yin
 */
public class AirplaneRecord {
    
    private ArrayList<Airplane> AirplaneRecord;

    private timeCount timeC;

    public timeCount getTimeC() {
        return timeC;
    }

    public void setTimeC(timeCount timeC) {
        this.timeC = timeC;
    }
//constructor 
    public AirplaneRecord(){
        AirplaneRecord = new ArrayList<Airplane>();
        timeC = new timeCount();
    }
          
    
    public ArrayList<Airplane> getAirplaneRecord() {
        return AirplaneRecord;
    }

    public void setAirplaneRecord(ArrayList<Airplane> AirplaneRecord) {
        this.AirplaneRecord = AirplaneRecord;
    }
    
    public Airplane addAirplane(){
        Airplane ap = new Airplane();
        AirplaneRecord.add(ap);
        return ap;
    }
    
    public void deleteAirplane(Airplane v){
    
            
            AirplaneRecord.remove(v);
            
    }
    
}
