/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 *
 * @author Yin
 */
public class TravelAgency {
    
    private String name;
    private UserDirectory userDirectory;
    

    public TravelAgency(String TAname) {
         this.name = TAname;
         userDirectory = new UserDirectory(TAname);
            
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserDirectory getUserDirectory() {
        return userDirectory;
    }

    public void setUserDirectory(UserDirectory userDirectory) {
        this.userDirectory = userDirectory;
    }
    
    

    
    
    
    
}
