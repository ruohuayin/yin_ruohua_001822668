/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Yin
 */
public class Flight {
    
    String flightNumber;
    String departureLoc;
    String landingLoc;
    String timeOfDay; 
    String date;
    double price;
    
    int SeatNumber;
    ArrayList<Customer> custList = new ArrayList();
    
    
    
   /* public void register(Customer c){
        custList.add(a);
    }*/
    
    
     public void registerSeat(Customer c) {
       custList.add(c);
       this.setSeatNumber(this.getSeatNumber()-1);
    }
    

    public int getSeatNumber() {
        return SeatNumber;
    }

    public void setSeatNumber(int SeatNumber) {
        this.SeatNumber = SeatNumber;
    }

    public ArrayList<Customer> getCustList() {
        return custList;
    }

    public void setCustList(ArrayList<Customer> custList) {
        this.custList = custList;
    }
    
    
    
    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getDepartureLoc() {
        return departureLoc;
    }

    public void setDepartureLoc(String departureLoc) {
        this.departureLoc = departureLoc;
    }

    public String getLandingLoc() {
        return landingLoc;
    }

    public void setLandingLoc(String landingLoc) {
        this.landingLoc = landingLoc;
    }

    public String getTimeOfDay() {
        return timeOfDay;
    }

    public void setTimeOfDay(String timeOfDay) {
        this.timeOfDay = timeOfDay;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    @Override
    public String toString(){
        return this.flightNumber;
    
    }

   
}
