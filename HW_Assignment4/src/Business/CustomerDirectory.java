/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author Yin
 */
public class CustomerDirectory {
    ArrayList<Customer> customerList;

  
    public CustomerDirectory(){
        customerList = new ArrayList();
    }

    public ArrayList<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(ArrayList<Customer> customerList) {
        this.customerList = customerList;
    }
    
    public Customer addCustomer(){
        Customer c = new Customer();
        customerList.add(c);
        return c;
        
    }
    
             
    public void loadCustomerDirectory(){
      try {
            BufferedReader reader = new BufferedReader(new FileReader("Data_csv/Customer.csv"));//换成你的文件名   
            String line = null; 
            while((line=reader.readLine())!=null){
                String Item = line;
               //String Item[] = line.split(",");
               Customer c = this.addCustomer();
               c.setName(Item);
               System.out.println(c.getName() + " is added to the customer Directory");
            }
        } catch (Exception e) {    
            e.printStackTrace();    
        }  
    }
             
            
             
    
}
