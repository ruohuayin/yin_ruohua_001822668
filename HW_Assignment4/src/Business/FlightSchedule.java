/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author Yin
 */
public class FlightSchedule {
    
    ArrayList<Flight> flightList;
    
    public FlightSchedule(){
        flightList = new ArrayList();
    }

    public ArrayList<Flight> getFlightList() {
        return flightList;
    }

    public void setFlightList(ArrayList<Flight> flightList) {
        this.flightList = flightList;
    }
    
    public Flight addFlight(){
        Flight f = new Flight();
        flightList.add(f);
        return f;
    }
    
    
    public void loadFlightSchedule(String airlinerAbbreName) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("Data_csv/"+airlinerAbbreName+"Flight.csv"));//换成你的文件名   
            String line = null; 
            while((line=reader.readLine())!=null){
               String item[] = line.split(",");
               Flight f = this.addFlight();
               f.setFlightNumber(item[0]);
               f.setDepartureLoc(item[1]);
               f.setLandingLoc(item[2]);
               f.setTimeOfDay(item[3]);
               f.setDate(item[4]);
               f.setPrice(Double.parseDouble(item[5])); 
               f.setSeatNumber(Integer.parseInt(item[6]));
               System.out.println(f.getFlightNumber() + " is added to the flightSchedule");
            }
        } catch (Exception e) {    
            e.printStackTrace();    
        }
        
    
    }
    
    
}
