/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author Yin
 */
public class AirlinerDirectory {
    
    ArrayList<Airliner> airlinerList;

    public AirlinerDirectory() {
        airlinerList = new ArrayList();
    }

    public ArrayList<Airliner> getAirlinerList() {
        return airlinerList;
    }

    public void setAirlinerList(ArrayList<Airliner> airlinerList) {
        this.airlinerList = airlinerList;
    }
    
    public Airliner addAirliner(){
        Airliner a = new Airliner();
        airlinerList.add(a);
        return a;
    }
    
    
    public void loadAirliners() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("Data_csv/Airliners.csv"));//换成你的文件名   
            String line = null; 
            while((line=reader.readLine())!=null){
               String Item[] = line.split(",");
               Airliner a = this.addAirliner();
               a.setName(Item[0]);
               a.setAbbreName(Item[1]);
               a.setFoundationYear(Item[2]);
               a.setPlaneNumber(Integer.parseInt(Item[3]));
               System.out.println(a.getName() + " is added to the AirlinerDirectory");
            }
        } catch (Exception e) {    
            e.printStackTrace();    
        }
        
    
    }
    
    
    
    
    
}
