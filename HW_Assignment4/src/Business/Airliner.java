/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Yin
 */
public class Airliner {
    
    private String name;
    private String abbreName;
    private String foundationYear;
    private int planeNumber;
    
    private FlightSchedule FS;

    public Airliner() {
        FS = new FlightSchedule();
    }

    public FlightSchedule getFS() {
        return FS;
    }

    public void setFS(FlightSchedule FS) {
        this.FS = FS;
    }
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreName() {
        return abbreName;
    }

    public void setAbbreName(String abbreName) {
        this.abbreName = abbreName;
    }

    public String getFoundationYear() {
        return foundationYear;
    }

    public void setFoundationYear(String foundationYear) {
        this.foundationYear = foundationYear;
    }

    public int getPlaneNumber() {
        return planeNumber;
    }

    public void setPlaneNumber(int planeNumber) {
        this.planeNumber = planeNumber;
    }
    
    @Override
    public String toString(){
        return this.getName();
    }
    
    
}
