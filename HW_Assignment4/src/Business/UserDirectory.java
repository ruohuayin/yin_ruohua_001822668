/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author Yin
 */
public class UserDirectory {
      
    ArrayList<User> userList;

    public UserDirectory(String TAname) {
        userList = new ArrayList();
        loadUsers(TAname);
    }
    
    
    public ArrayList<User> getUserList() {
        return userList;
    }

    public void setUserList(ArrayList<User> userList) {
        this.userList = userList;
    }

    public User addUser(){
        User u = new User();
        userList.add(u);
        return u;
    }
    
    private void loadUsers(String TAname) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("Data_csv/"+TAname+"Users.csv"));//换成你的文件名   
            String line = null; 
            while((line=reader.readLine())!=null){
               String Item[] = line.split(",");
               User u = this.addUser();
               u.setName(Item[0]);
               System.out.println(u.getName());
            }
        } catch (Exception e) {    
            e.printStackTrace();    
        }
        
    
    }
    
    
    
    
    
}
