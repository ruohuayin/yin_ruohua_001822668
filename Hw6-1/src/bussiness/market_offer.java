/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bussiness;

/**
 *
 * @author qwe09
 */
public class market_offer {
    product p;
    market m;
    int floor;
    int ceiling;
    int target;

    public product getP() {
        return p;
    }

    public void setP(product p) {
        this.p = p;
    }

    public market getM() {
        return m;
    }

    public void setM(market m) {
        this.m = m;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getCeiling() {
        return ceiling;
    }

    public void setCeiling(int ceiling) {
        this.ceiling = ceiling;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }
    
    
}
