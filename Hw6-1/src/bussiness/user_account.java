/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bussiness;

/**
 *
 * @author qwe09
 */
public class user_account {
    person p;
    String password;
    String name;
    String role;

    public person getP() {
        return p;
    }

    public void setP(person p) {
        this.p = p;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
     public String toString(){
        return this.getName();
    }
    
    
}
