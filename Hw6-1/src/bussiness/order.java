/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bussiness;

import java.util.ArrayList;

/**
 *
 * @author qwe09
 */
public class order {
    String id;
    int total_price;
    String market;
    ArrayList<order_item> oia;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getTotal_price() {
        return total_price;
    }

    public void setTotal_price(int total_price) {
        this.total_price = total_price;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public ArrayList<order_item> getOia() {
        return oia;
    }

    public void setOia(ArrayList<order_item> oia) {
        this.oia = oia;
    }
    
    
}
