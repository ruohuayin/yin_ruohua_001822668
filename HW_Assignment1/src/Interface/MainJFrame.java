/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Person_info.Person;

/**
 *
 * @author Yin
 */
public class MainJFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainJFrame
     */
    
    private Person person;
    private Person Sperson;
    
    public MainJFrame() {
        initComponents();
        person = new Person();
        Sperson = new Person();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        splitPane = new javax.swing.JSplitPane();
        createPanel = new javax.swing.JPanel();
        createPersonBtn = new javax.swing.JButton();
        viewPersonBtn = new javax.swing.JButton();
        createSpouseBtn = new javax.swing.JButton();
        viewSpouseBtn = new javax.swing.JButton();
        viewPanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        createPersonBtn.setText("Person");
        createPersonBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createPersonBtnActionPerformed(evt);
            }
        });

        viewPersonBtn.setText("View ↑");
        viewPersonBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewPersonBtnActionPerformed(evt);
            }
        });

        createSpouseBtn.setFont(new java.awt.Font("Lucida Grande", 0, 12)); // NOI18N
        createSpouseBtn.setText("Spouse");
        createSpouseBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createSpouseBtnActionPerformed(evt);
            }
        });

        viewSpouseBtn.setText("View ↑");
        viewSpouseBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewSpouseBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout createPanelLayout = new javax.swing.GroupLayout(createPanel);
        createPanel.setLayout(createPanelLayout);
        createPanelLayout.setHorizontalGroup(
            createPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(createPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(createPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(createSpouseBtn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(viewPersonBtn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(createPersonBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(viewSpouseBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(17, Short.MAX_VALUE))
        );
        createPanelLayout.setVerticalGroup(
            createPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(createPanelLayout.createSequentialGroup()
                .addGap(119, 119, 119)
                .addComponent(createPersonBtn)
                .addGap(5, 5, 5)
                .addComponent(viewPersonBtn)
                .addGap(53, 53, 53)
                .addComponent(createSpouseBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(viewSpouseBtn)
                .addContainerGap(109, Short.MAX_VALUE))
        );

        splitPane.setLeftComponent(createPanel);

        viewPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        splitPane.setRightComponent(viewPanel);

        getContentPane().add(splitPane, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void viewPersonBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewPersonBtnActionPerformed
        // TODO add your handling code here:
        ViewPanel viewPanel = new ViewPanel(person);
        splitPane.setRightComponent(viewPanel);
        setBounds(0,0,400,900);
      //  System.out.println(person.getLicense().)
      
      
    }//GEN-LAST:event_viewPersonBtnActionPerformed

    private void createPersonBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createPersonBtnActionPerformed
        // TODO add your handling code here:
        CreatePanel createPanel = new CreatePanel(person);
        splitPane.setRightComponent(createPanel);
        setBounds(2,2,500,420);
        
         
    }//GEN-LAST:event_createPersonBtnActionPerformed

    private void createSpouseBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createSpouseBtnActionPerformed
        // TODO add your handling code here:
        CreateSpousePanel createSPanel = new CreateSpousePanel(Sperson);
        splitPane.setRightComponent(createSPanel);
        setBounds(2,2,500,420);
        
    }//GEN-LAST:event_createSpouseBtnActionPerformed

    private void viewSpouseBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewSpouseBtnActionPerformed
        // TODO add your handling code here:
        ViewSpousePanel viewSPanel = new ViewSpousePanel(Sperson);
        splitPane.setRightComponent(viewSPanel);
        setBounds(0,0,400,900);
    }//GEN-LAST:event_viewSpouseBtnActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JPanel createPanel;
    private javax.swing.JButton createPersonBtn;
    private javax.swing.JButton createSpouseBtn;
    private javax.swing.JSplitPane splitPane;
    private javax.swing.JPanel viewPanel;
    private javax.swing.JButton viewPersonBtn;
    private javax.swing.JButton viewSpouseBtn;
    // End of variables declaration//GEN-END:variables
}
