/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Person_info;

/**
 *
 * @author Yin
 */
public class Financial {
    String checkingNumber;
    String checkingCreationDate;
    String checkingActive;
    String checkingType;
    
    String savingNumber;
    String savingCreationDate;
    String savingActive;
    String savingType;

    public String getCheckingNumber() {
        return checkingNumber;
    }

    public void setCheckingNumber(String checkingNumber) {
        this.checkingNumber = checkingNumber;
    }

    public String getCheckingCreationDate() {
        return checkingCreationDate;
    }

    public void setCheckingCreationDate(String checkingCreationDate) {
        this.checkingCreationDate = checkingCreationDate;
    }

    public String getCheckingActive() {
        return checkingActive;
    }

    public void setCheckingActive(String checkingActive) {
        this.checkingActive = checkingActive;
    }

    public String getCheckingType() {
        return checkingType;
    }

    public void setCheckingType(String checkingType) {
        this.checkingType = checkingType;
    }

    public String getSavingNumber() {
        return savingNumber;
    }

    public void setSavingNumber(String savingNumber) {
        this.savingNumber = savingNumber;
    }

    public String getSavingCreationDate() {
        return savingCreationDate;
    }

    public void setSavingCreationDate(String savingCreationDate) {
        this.savingCreationDate = savingCreationDate;
    }

    public String getSavingActive() {
        return savingActive;
    }

    public void setSavingActive(String savingActive) {
        this.savingActive = savingActive;
    }

    public String getSavingType() {
        return savingType;
    }

    public void setSavingType(String savingType) {
        this.savingType = savingType;
    }
    
}
